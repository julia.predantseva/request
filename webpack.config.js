const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const publicPath = path.resolve(__dirname, 'public');

module.exports = {
  mode: 'development',
  entry: {
    app: ['./src/index.js']
  },
  devtool: 'inline-source-map',
  devServer: {
    contentBase: publicPath
  },
  plugins: [
    new CleanWebpackPlugin({ cleanOnceBeforeBuildPatterns: ['public'] }),
    new HtmlWebpackPlugin({
      title: 'Internship',
      template: path.resolve(__dirname, 'index.ejs')
    })
  ],
  output: {
    filename: '[name].bundle.js',
    path: publicPath
  }
};